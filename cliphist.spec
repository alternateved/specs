%global debug_package %{nil}

Name:     cliphist
Version:  0.6.1
Release:  %autorelease
Summary:  Wayland clipboard manager with support for multimedia
License:  GPL-3.0-only
URL:      https://github.com/sentriz/cliphist
Source0:  %{url}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires: go
BuildRequires: git
Requires: glibc
Requires: wl-clipboard
Requires: xdg-utils

%description
Wayland clipboard manager with support for multimedia.

%gopkg

%prep
%autosetup

%build
go build -ldflags "-linkmode external -extldflags '$LDFLAGS'" -o %{name}

%install
install -Dm0755 -t %{buildroot}%{_bindir}/ %{name}
install -Dm0755 contrib/* -t %{buildroot}%{_datadir}/%{name}
install -Dm0644 LICENSE -t %{buildroot}%{_licensedir}/%{name}
install -Dm0644 readme.md -t %{buildroot}%{_docdir}/%{name}

%files
%doc readme.md
%license %{_licensedir}/%{name}/LICENSE
%{_bindir}/%{name}
%{_datadir}/%{name}

%changelog
%autochangelog
