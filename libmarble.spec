%global         forgeurl https://gitlab.gnome.org/raggesilver/marble
%global         commit 6dcc6fefa35f0151b0549c01bd774750fe6bdef8

Name:           libmarble
Version:        42~alpha0
Release:        %autorelease
Summary:        A collection of useful functions and reusable widgets

%forgemeta

License:        GPLv3+
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  meson
BuildRequires:  vala
BuildRequires:  pkgconfig(gio-2.0) >= 2.50
BuildRequires:  pkgconfig(gtk4) >= 4.6

%description
A collection of useful functions and reusable widgets.


%package devel
Summary:        Development files for libmarble

Requires:       vala
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Development files for libmarble.


%prep
%forgeautosetup


%build
%meson
%meson_build


%install
%meson_install


%files
%license COPYING
%doc README.md
%{_libdir}/libmarble.so.42
%{_libdir}/girepository-1.0/*.typelib

%files devel
%{_libdir}/libmarble.so
%{_includedir}/marble.h
%{_libdir}/pkgconfig/marble.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/*.gir
%{_datadir}/vala/vapi/marble.{deps,vapi}


%changelog
%autochangelog
