%global debug_package %{nil}
%global forgeurl https://gitlab.com/akkuscm/akku
%global commit 36cfb713a3011d80fd329757c67509992cacef5b

%forgemeta

Name:     akku
Version:  1.1.0
Release:  %autorelease
Summary:  Language package manager for Scheme
License:  GPLv3+
URL:	    %{forgeurl}
Source:   %{forgesource}

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  make
BuildRequires:  curl
BuildRequires:  guile-devel
BuildRequires:  pkgconfig
BuildRequires:  pkgconfig(guile-3.0)
Requires:       guile >= 3.0

%description
Akku.scm is a language package manager for Scheme. It grabs hold of code and
vigorously shakes it until it behaves properly.

%prep
%forgeautosetup

%build
sed -i -E 's/(^|[^.])guile(\s|;)/\1guile3.0\2/g' bootstrap 
./bootstrap
%configure GUILD=/usr/bin/guild3.0 GUILE_CONFIG=/usr/bin/guile-config3.0
%make_build

%install
%make_install

%files
%license COPYING
%doc CONTRIBUTING.md CREDITS.md NEWS.md README.md
%{_bindir}/akku
%{_libdir}/akku
%{_datadir}/akku
%{_mandir}/man1/akku.1.*

%changelog
%autochangelog
