%global pypi_name safeeyes
%global pypi_version 2.2.2

Name:           safeeyes
Version:        %{pypi_version}
Release:        1%{?dist}
Summary:        Protect your eyes from eye strain using this continuous breaks reminder

License:        None
URL:            https://github.com/slgobinath/SafeEyes
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)
BuildRequires:  gettext

Requires:       python3dist(babel)
Requires:       python3dist(croniter)
Requires:       python3dist(packaging)
Requires:       python3dist(psutil)
Requires:       python3dist(pygobject)
Requires:       python3dist(python-xlib)
Requires:       python3dist(setuptools)

%description
Safe Eyes is a Free and Open Source tool for Linux users to reduce and prevent repetitive strain injury (RSI).

%prep
%autosetup -n %{pypi_name}-%{pypi_version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files
%license LICENSE
%doc README.md
%{_bindir}/safeeyes
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{pypi_version}-py%{python3_version}.egg-info
%{_datadir}/applications/*
%{_datadir}/icons/hicolor/16x16/apps/*
%{_datadir}/icons/hicolor/24x24/apps/*
%{_datadir}/icons/hicolor/32x32/apps/*
%{_datadir}/icons/hicolor/48x48/apps/*
%{_datadir}/icons/hicolor/64x64/apps/*
%{_datadir}/icons/hicolor/128x128/apps/*
%{_datadir}/icons/hicolor/16x16/status/*
%{_datadir}/icons/hicolor/24x24/status/*
%{_datadir}/icons/hicolor/32x32/status/*
%{_datadir}/icons/hicolor/48x48/status/*

%changelog
* Sun Aug 25 2024 Tomasz Hołubowicz <mail@alternateved.com> - 2.2.2-1
- Initial package.
