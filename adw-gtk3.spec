Name:          adw-gtk3
Version:       4.7
Release:       1%{?dist}
Summary:       The theme from libadwaita ported to GTK-3
BuildArch:     noarch
License:       LGPLv2+
URL:           https://github.com/lassekongo83/adw-gtk3
Source:        %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires: sassc
BuildRequires: meson
BuildRequires: ninja-build

%description
The theme from libadwaita ported to GTK-3

%prep
%autosetup

%build
%meson
%meson_build

%install
%meson_install

%files
%license LICENSE
%{_datadir}/themes/adw-gtk3/*
%{_datadir}/themes/adw-gtk3-dark/*
