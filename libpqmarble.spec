%global git_sha_version f240b2ec7d5cdacb8fdcc553703420dc5101ffdb

Name:           libpqmarble
Version:        2.0.0
Release:        1%{?dist}
Summary:        Utility library for GNOME apps.

License:        GPLv3
URL:            https://gitlab.gnome.org/raggesilver/marble
Source0:        https://gitlab.gnome.org/raggesilver/marble/-/archive/%{git_sha_version}/marble-%{git_sha_version}.tar.bz2

BuildRequires:  meson
BuildRequires:  gcc
BuildRequires:  vala
BuildRequires:  pkgconfig(gtk4)

%description
Utility library for GNOME apps.


%package vala
Summary:        Vala bindings for libpqmarble
Requires:       %{name} = %{version}-%{release}
Requires:       vala
BuildArch:      noarch

%description vala
Vala bindings for libpqmarble.


%package devel
Summary:        Development files for libpqmarble
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	%{name}-vala = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n marble-%{git_sha_version}


%build
%meson
%meson_build

%install
%meson_install


%files
%license COPYING
%doc README.md
%{_libdir}/libpqmarble.so.2
%{_libdir}/libpqmarble.so.2.*
%{_libdir}/girepository-1.0/PQMarble-2.typelib

%files vala
%{_datadir}/vala/vapi/pqmarble.vapi
%{_datadir}/vala/vapi/pqmarble.deps

%files devel
%{_includedir}/pqmarble.h
%{_libdir}/libpqmarble.so
%{_libdir}/pkgconfig/pqmarble.pc
%{_datadir}/gir-1.0/PQMarble-2.gir


%changelog
* Thu Jun 01 2023 Olivier Samyn <code@oleastre.be>
-
