%global debug_package %{nil}
%global forgeurl https://github.com/greshake/i3status-rust
%global commit 91179683f6fed833d3557a7cf3928d8707d4a252
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%forgemeta

Name:           i3status-rust
Version:        0.33.1
Release:        1.git%{shortcommit}%{?dist}
Summary:        Resourcefriendly and feature-rich replacement for i3status, written in Rust

License:        GPL-3.0-or-later
URL:	          %{forgeurl}
Source:         %{forgesource}

BuildRequires:  gcc
BuildRequires:  clang
BuildRequires:  lm_sensors-devel
%if 0%{?el8} || 0%{?el9}
%else
BuildRequires:  cargo >= 1.67
BuildRequires:  pandoc
BuildRequires:  rust >= 1.67
%endif

BuildRequires:  pkgconfig(dbus-1)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libpipewire-0.3)
BuildRequires:  pkgconfig(openssl)

%if 0%{?fedora} || 0%{?rhel} >= 8
Recommends:     fontawesome-fonts

Enhances:       i3
Enhances:       sway
%endif

Conflicts:      i3status-rs

%description
i3status-rs is a feature-rich and resource-friendly replacement for i3status,
written in pure Rust. It provides a way to display "blocks" of system
information (time, battery status, volume, etc) on bars that support the i3bar
protocol.

%prep
%forgeautosetup

%if 0%{?el8} || 0%{?el9}
curl https://sh.rustup.rs -sSf | sh -s -- --profile minimal -y
%endif

# Set codegen-units to 1
echo 'codegen-units = 1' >> Cargo.toml


%install
export CARGO_PROFILE_RELEASE_BUILD_OVERRIDE_OPT_LEVEL=3

%if 0%{?el8} || 0%{?el9}
source "$HOME/.cargo/env"
%endif
cargo install --features pipewire --root=%{buildroot}%{_prefix} --path=. --locked
%if 0%{?fedora}
cargo xtask generate-manpage
%endif

rm -f %{buildroot}%{_prefix}/.crates.toml \
    %{buildroot}%{_prefix}/.crates2.json
strip --strip-all %{buildroot}%{_bindir}/*

mkdir -p %{buildroot}%{_datadir}/%{name}
cp -r files/* %{buildroot}%{_datadir}/%{name}/
%if 0%{?fedora}
install -D -p -m 0644 man/i3status-rs.1 %{buildroot}%{_mandir}/man1/i3status-rs.1
%endif


%files
%license LICENSE
%doc README.md NEWS.md CONTRIBUTING.md examples/ doc/*
%{_bindir}/i3status-rs
%{_datadir}/%{name}/
%if 0%{?fedora}
%{_mandir}/man1/*.1*
%endif


%changelog
* Sat Jun 08 2024 Tomasz Hołubowicz <mail@alternateved.com> - 0.33.1-1
- Enable pipewire feature

* Mon Apr 08 2024 Artem Polishchuk <ego.cordatus@gmail.com> - 0.33.1-1
- chore: Update to latest release

* Mon Feb 19 2024 Artem Polishchuk <ego.cordatus@gmail.com> - 0.33.0-1
- chore: Update to latest release

* Wed Nov 22 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.32.3-1
- chore(update): 0.32.3

* Sat Sep 09 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.32.2-1
- chore(update): 0.32.2

* Sat Sep 02 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.32.1-1
- chore(update): 0.32.1

* Fri Sep 01 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.32.0-1
- chore(update): 0.32.0

* Mon Jul 31 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.31.9-1
- chore(update): 0.31.9

* Tue Jun 13 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.31.7-1
- chore(update): 0.31.7

* Thu Jun 01 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.31.6-1
- chore(update): 0.31.6

* Mon May 15 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.31.4-1
- chore(update): 0.31.4

* Sun May 07 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.31.2-1
- chore(update): 0.31.2

* Mon May 01 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.31.1-1
- chore(update): 0.31.1

* Fri Apr 28 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.31.0-1
- chore(update): 0.31.0

* Wed Apr 12 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.30.7-1
- chore(update): 0.30.7

* Fri Mar 24 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.30.6-1
- chore(update): 0.30.6

* Wed Feb 22 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.30.2-1
- chore(update): 0.30.2

* Tue Feb 21 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.30.1-1
- chore(update): 0.30.1

* Tue Feb 21 2023 Artem Polishchuk <ego.cordatus@gmail.com> - 0.30.0-1
- chore(update): 0.30.0

* Tue Jun 21 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.11-1
- chore(update): 0.21.11

* Tue Apr 19 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.10-1
- chore(update): 0.21.10

* Mon Apr 04 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.9-1
- chore(update): 0.21.9

* Tue Mar 15 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.8-1
- chore(update): 0.21.8

* Tue Mar 01 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.7-1
- chore(update): 0.21.7

* Mon Feb 14 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.6-1
- chore(update): 0.21.6

* Fri Feb 11 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.5-1
- chore(update): 0.21.5

* Tue Feb 01 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.4-1
- chore(update): 0.21.4

* Sun Jan 30 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.3-1
- chore(update): 0.21.3

* Thu Jan 20 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.2-1
- chore(update): 0.21.2

* Tue Jan 18 2022 Artem Polishchuk <ego.cordatus@gmail.com> - 0.21.1-1
- chore(update): 0.21.1

* Fri Dec 03 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.20.7-1
- chore(update): 0.20.7

* Thu Nov 11 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.20.6-1
- chore(update): 0.20.6

* Sun Nov 07 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.20.5-1
- chore(update): 0.20.5

* Tue Jun 29 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.20.2-1
- build(update): 0.20.2

* Thu May 13 2021 Jerzy Drozdz <jerzy.drozdz@gmail.com> - 0.20.1-2
- added installation of themes

* Wed May 12 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.20.1-1
- build(update): 0.20.1

* Sun May 09 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.20.0-1
- build(update): 0.20.0

* Mon Feb 15 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.14.7-1
- build(update): 0.14.7

* Sun Feb 14 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 0.14.6-1
- build(update): 0.14.6

* Thu Dec  3 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.14.3-1
- build(update): 0.14.3

* Sat Oct 31 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.14.2-1
- build(update): 0.14.2

* Mon Jun 08 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.14.1-1
- Update to 0.14.1

* Sun Jun 07 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.14.0-1
- Update to 0.14.0

* Sun May 24 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.13.1-2
- Add patch for nvidia gpu block | GH-278

* Fri Feb 07 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.13.1-1
- Update to 0.13.1

* Mon Feb 03 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.13.0-2
- Add config examples
- Use Enhances instead of Suggests

* Mon Feb 03 2020 Artem Polishchuk <ego.cordatus@gmail.com> - 0.13.0-1
- Update to 0.13.0

* Mon Dec 09 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.12.0-1
- Update to 0.12.0

* Tue Nov 05 2019 Artem Polishchuk <ego.cordatus@gmail.com> - 0.11.0-3
- Initial package
