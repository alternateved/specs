%global debug_package %{nil}

Name: keyd
Version: 2.5.0
Release: 3%{?dist}
Summary: A powerful key remapper for the Linux desktop

License: MIT
URL: https://github.com/rvaiya/%{name}
Source0: %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires: gcc-c++
BuildRequires: kernel-headers

%description
%{summary}

%prep
%autosetup

%build
%make_build PREFIX="/usr"

%install
%make_install PREFIX="/usr"
install -Dm644 keyd.service -t "%{buildroot}/usr/lib/systemd/system/"
install -Dm755 scripts/dump-xkb-config -t "%{buildroot}/usr/share/keyd/"
install -Dm755 scripts/generate_xcompose -t "%{buildroot}/usr/share/keyd/"
install -Dm644 LICENSE -t "%{buildroot}/usr/share/licenses/keyd"
install -Dm644 README.md -t "%{buildroot}/usr/share/doc/keyd/"
echo 'g keyd' | install -Dm644 /dev/stdin "%{buildroot}/usr/lib/sysusers.d/%{name}.conf"

%files
%{_bindir}/%{name}
%{_bindir}/%{name}-application-mapper
%{_docdir}/%{name}
%{_datadir}/%{name}

%{_prefix}/lib/systemd/system/%{name}.service
%{_prefix}/lib/sysusers.d/%{name}.conf

%{_mandir}/man1/%{name}.1.gz
%{_mandir}/man1/%{name}-application-mapper.1.gz
%license LICENSE

%changelog
%autochangelog
