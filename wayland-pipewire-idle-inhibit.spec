%bcond_without check

Name:           wayland-pipewire-idle-inhibit
Version:        0.5.2
Release:        %autorelease
Summary:        Inhibit wayland idle when computer is playing sound

License:        GPL-3.0-only and Apache-2.0 and Apache-2.0 WITH LLVM-exception and BSD-2-Clause and ISC and MIT and MPL-2.0 and Unicode-DFS-2016 and Unlicense and Zlib
URL:            https://crates.io/crates/wayland-pipewire-idle-inhibit
Source:         %{crates_source}

BuildRequires:  cargo-rpm-macros >= 24

%description
Inhibit wayland idle when computer is playing sound.

%files
%license LICENCE LICENSE.dependencies
%doc README.md
%{_bindir}/wayland-pipewire-idle-inhibit

%prep
%autosetup -n %{name}-%{version} -p1
%cargo_prep

%generate_buildrequires
%cargo_generate_buildrequires

%build
%cargo_build
%{cargo_license} > LICENSE.dependencies

%install
%cargo_install

%if %{with check}
%check
%cargo_test
%endif

%changelog
%autochangelog
