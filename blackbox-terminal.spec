%global         forgeurl https://gitlab.gnome.org/raggesilver/blackbox
%global         appname blackbox
%global         appid com.raggesilver.BlackBox

Name:           blackbox-terminal
Version:        0.14.0
Release:        %autorelease
Summary:        A beautiful GTK 4 terminal

%global         tag v%{version}
%forgemeta

License:        GPL-3.0-or-later AND Apache-2.0
URL:            %{forgeurl}
Source0:        %{forgesource}
BuildRequires:  git-core

BuildRequires:  meson
BuildRequires:  vala
BuildRequires:  desktop-file-utils
BuildRequires:  intltool
BuildRequires:  libappstream-glib
BuildRequires:  pkgconfig(gio-2.0) >= 2.50
BuildRequires:  pkgconfig(gtk4) >= 4.6.2
BuildRequires:  pkgconfig(libadwaita-1) >= 1.2
BuildRequires:  pkgconfig(marble) >= 2.0.0
BuildRequires:  pkgconfig(pqmarble)
BuildRequires:  pkgconfig(vte-2.91-gtk4) >= 0.69.0
BuildRequires:  pkgconfig(json-glib-1.0) >= 1.4.4
BuildRequires:  pkgconfig(libpcre2-8)
BuildRequires:  pkgconfig(libxml-2.0) >= 2.9.12
BuildRequires:  pkgconfig(librsvg-2.0) >= 2.54.0
BuildRequires:  pkgconfig(graphene-gobject-1.0)
BuildRequires:  pkgconfig(gee-0.8)
Requires:       hicolor-icon-theme

# Same name... Conflicting binary files.
Conflicts:      blackbox

%description
A beautiful GTK 4 terminal.

Features:
* Theming (Tilix compatible color scheme support)
* Theme integration with the window decorations
* Custom fonts
* Tabs
* Headerbarless mode
* Ctrl + click to open links & files
* Drag files to paste their path


%prep
%forgeautosetup -S git


%build
export CFLAGS="$RPM_OPT_FLAGS"
%meson
%meson_build


%install
%meson_install
%find_lang %{appname}


%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop


%files -f %{appname}.lang
%license COPYING
%doc README.md CHANGELOG.md
%{_bindir}/%{appname}
%{_bindir}/terminal-toolbox
%{_datadir}/applications/%{appid}.desktop
%{_datadir}/%{appname}
%{_datadir}/glib-2.0/schemas/%{appid}.gschema.xml
%{_datadir}/icons/hicolor/scalable/*/*.svg
%{_metainfodir}/%{appid}.metainfo.xml


%changelog
%autochangelog
